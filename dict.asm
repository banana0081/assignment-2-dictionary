
%include "lib.inc"
global find_word
section .text
; rdi contains pointer to the key, rsi - polinter to the first element
; returns pointer to the found value in rax, 0 if none found

find_word:
    .loop:
    push rdi ; save key
    push rsi; save beginning
    add rsi, 8 ; rsi+8 contains value
    call string_equals; compare key to the value
    cmp rax, 1
    pop rsi
    pop rdi
    je .found;  if strings are equal the key is found
    mov rsi, [rsi] ; next element in rsi
    cmp rsi, 0 ; if points to 0 - no next element present
    je .not_found
    jne .loop ; if it does not - try the next key
    
    .found:
        mov rax, rsi ; return the pointer
        ret
    .not_found:
        xor rax, rax ; return 0
        ret
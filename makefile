ASM=nasm
ASMFLAGS=-f elf64
LD=ld

program: main.o lib.o dict.o
	$(LD) -o $@ $^
main.o: main.asm lib.asm colon.inc words.inc
	$(ASM) -g $< $(ASMFLAGS) -o $@
lib.o: lib.asm
	$(ASM) -g $< $(ASMFLAGS) -o $@
dict.o: dict.asm lib.inc
	$(ASM) -g $< $(ASMFLAGS) -o $@

	
.PHONY: all, clean
all: main
clean:
	rm ./*.o
	rm ./program

section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

; Принимает код возврата и завершает текущий процесс
exit: 
     mov rax, 60
     syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    .loop:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rax, rdi
    sub rsp, 1
    mov byte[rsp], al
    mov rsi, rsp
    mov rdi, 1
    mov rax, 1
    mov rdx, 1
    syscall
    add rsp, 1
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0x0A
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, rsp
    sub rsp, 1
    mov byte[rsp], 0x00
    .loop:
    	xor rdx, rdx
    	xor r10, r10
    	mov r11, 0x0A
    	div r11
    	add dx, 0x0030
    	mov r10, rax
    	mov rax, rdx
    	sub rsp, 1
    	mov byte[rsp], al
    	mov rax, r10
    	cmp rax, 0x0
    	jnz .loop
    .out:
    	mov rdi, rsp
        push rdi
        push rax
    	call print_string
        pop rax
        pop rdi
    	mov rdi, rsp
        push rdi
    	call string_length
        pop rdi
    	add rsp, rax
    	inc rsp
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .n
    .p:
        jmp print_uint
    .n:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    .A:
        mov r8b, byte[rdi]
        mov r9b, byte[rsi]
        cmp r8b, r9b
        jne .C
        cmp r8b, 0
        je .B
        inc rdi
        inc rsi
        jmp .A
    .B:
        mov rax, 1
        ret
    .C:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp

    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor r9, r9
    .loop:
        push r9
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop r9
        
        cmp rax, 0
        je .quit

        cmp al, 0x0020
        je .skip
        cmp al, 0x0009
        je .skip
        cmp al, 0x000A
        je .skip

        mov [rdi+r9], rax
        inc r9
        cmp r9, rsi
        jge .err

        jmp .loop
    .skip:
        cmp r9, 0
        je .loop
        jmp .quit
    .err:
        xor rax, rax
        xor rdx, rdx
        ret
    .quit:
        xor rax, rax
        mov [rdi+r9], rax
        mov rax, rdi
        mov rdx, r9
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rax, rax
mov rcx, rdi
mov r11, 10
    .A:
      movzx r8, byte[rdi]

      cmp r8, 0x0
      je .B
      cmp r8b, 0x30
      jl .B
      cmp r8b, 0x39
      jg .B

      mul r11
      sub r8, 0x30
      add rax, r8
      inc rdi
      jmp .A
    .B:
      sub rcx, rdi
      neg rcx
      mov rdx, rcx
      ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .n
    jmp .p
    .p:
        jmp parse_uint
    .n:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9 ; r9 contains offset

    push rsi 
    push rdi
    push r9 
    push rdx
    call string_length
    pop rdx
    pop r9 
    pop rdi
    pop rsi

    mov r10, rax; r10 contains len
    cmp rdx, r10
    jl .err

    .loop:
        cmp r9, r10
        jg .end

        mov r8, [rdi+r9] ;moving from the string
        mov [rsi+r9], r8 ;to the buffer
        
        inc r9
        jmp .loop
    .err:
        xor rax, rax
        ret
    .end:
        mov rax, r10
        ret

print_error:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
    syscall
    mov rdi, 1
    call exit
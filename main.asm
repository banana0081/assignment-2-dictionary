%include "dict.inc"

section .rodata
    string_too_long_err: db "String was too long, length must not exceed 255 characters.", `\n`, 0
    key_not_found_err: db "Key was not found", `\n`, 0
    enter_key: db "Enter key: ", 0
    success_message: db "Found entry: ", 0
section .text



_start:
    mov rdi, enter_key
    call print_string
    mov rsi, size_limit
    sub rsp, rsi
    mov rdi, rsp
    call read_word
    cmp rax, 0
    je .string_too_long   ; check for appropriate length
    ; addr in rax, len in rdx
    mov rdi, rax 
    mov rsi, pointer    
    ; rdi, rsi - key, dict
    call find_word ; returns 0 if not found, else address

    add rsp, size_limit
    test rax, rax
    je .not_found  ; if 0 - word is not in dict
    ; else 
    jne .print_success
    
.string_too_long:
    mov rdi, string_too_long_err
    jmp print_error
.not_found:
    mov rdi, key_not_found_err
    jmp print_error

.print_success:
    push rax; save the address
    mov rdi, success_message
    call print_string ; print the success message
    pop rax
    mov rdi, rax 
    add rdi, 8 ; skipping the first 8 bytes (pointer to the next element)
    push rdi 
    call string_length
    pop rdi
    add rdi, rax
    inc rdi ; skip the key to get to the value (not forgetting the null at the end)
    call print_string
    call print_newline
    mov rdi, 0 ; success code
    jmp exit